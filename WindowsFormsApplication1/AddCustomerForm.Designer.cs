﻿namespace WinFormsCustomersApp
{
    partial class AddCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CustomerTypeDrop = new System.Windows.Forms.ComboBox();
            this.CustomerTypeLabel = new System.Windows.Forms.Label();
            this.AddCustName = new System.Windows.Forms.Label();
            this.AddCustAddress = new System.Windows.Forms.Label();
            this.AddCustPhone = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.SpecificLabel1 = new System.Windows.Forms.Label();
            this.SpecificLabel2 = new System.Windows.Forms.Label();
            this.SpecificTextBox1 = new System.Windows.Forms.TextBox();
            this.SpecificTextBox2 = new System.Windows.Forms.TextBox();
            this.FinishAddButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CustomerTypeDrop
            // 
            this.CustomerTypeDrop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustomerTypeDrop.FormattingEnabled = true;
            this.CustomerTypeDrop.Items.AddRange(new object[] {
            "Customer",
            "Individual",
            "Company"});
            this.CustomerTypeDrop.Location = new System.Drawing.Point(12, 29);
            this.CustomerTypeDrop.Name = "CustomerTypeDrop";
            this.CustomerTypeDrop.Size = new System.Drawing.Size(103, 21);
            this.CustomerTypeDrop.TabIndex = 0;
            this.CustomerTypeDrop.SelectedIndexChanged += new System.EventHandler(this.CustomerTypeDrop_SelectedIndexChanged);
            // 
            // CustomerTypeLabel
            // 
            this.CustomerTypeLabel.AutoSize = true;
            this.CustomerTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerTypeLabel.Location = new System.Drawing.Point(12, 10);
            this.CustomerTypeLabel.Name = "CustomerTypeLabel";
            this.CustomerTypeLabel.Size = new System.Drawing.Size(103, 16);
            this.CustomerTypeLabel.TabIndex = 1;
            this.CustomerTypeLabel.Text = "Customer Type:";
            // 
            // AddCustName
            // 
            this.AddCustName.AutoSize = true;
            this.AddCustName.Location = new System.Drawing.Point(12, 56);
            this.AddCustName.Name = "AddCustName";
            this.AddCustName.Size = new System.Drawing.Size(38, 13);
            this.AddCustName.TabIndex = 2;
            this.AddCustName.Text = "Name:";
            // 
            // AddCustAddress
            // 
            this.AddCustAddress.AutoSize = true;
            this.AddCustAddress.Location = new System.Drawing.Point(12, 105);
            this.AddCustAddress.Name = "AddCustAddress";
            this.AddCustAddress.Size = new System.Drawing.Size(48, 13);
            this.AddCustAddress.TabIndex = 3;
            this.AddCustAddress.Text = "Address:";
            // 
            // AddCustPhone
            // 
            this.AddCustPhone.AutoSize = true;
            this.AddCustPhone.Location = new System.Drawing.Point(12, 160);
            this.AddCustPhone.Name = "AddCustPhone";
            this.AddCustPhone.Size = new System.Drawing.Size(41, 13);
            this.AddCustPhone.TabIndex = 4;
            this.AddCustPhone.Text = "Phone:";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(15, 72);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(100, 20);
            this.NameTextBox.TabIndex = 5;
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Location = new System.Drawing.Point(15, 121);
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(100, 20);
            this.AddressTextBox.TabIndex = 6;
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Location = new System.Drawing.Point(15, 176);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(100, 20);
            this.PhoneTextBox.TabIndex = 7;
            // 
            // SpecificLabel1
            // 
            this.SpecificLabel1.AutoSize = true;
            this.SpecificLabel1.Location = new System.Drawing.Point(140, 56);
            this.SpecificLabel1.Name = "SpecificLabel1";
            this.SpecificLabel1.Size = new System.Drawing.Size(87, 13);
            this.SpecificLabel1.TabIndex = 8;
            this.SpecificLabel1.Text = "License Number:";
            // 
            // SpecificLabel2
            // 
            this.SpecificLabel2.AutoSize = true;
            this.SpecificLabel2.Location = new System.Drawing.Point(140, 105);
            this.SpecificLabel2.Name = "SpecificLabel2";
            this.SpecificLabel2.Size = new System.Drawing.Size(35, 13);
            this.SpecificLabel2.TabIndex = 9;
            this.SpecificLabel2.Text = "label1";
            // 
            // SpecificTextBox1
            // 
            this.SpecificTextBox1.Location = new System.Drawing.Point(143, 71);
            this.SpecificTextBox1.Name = "SpecificTextBox1";
            this.SpecificTextBox1.Size = new System.Drawing.Size(100, 20);
            this.SpecificTextBox1.TabIndex = 10;
            // 
            // SpecificTextBox2
            // 
            this.SpecificTextBox2.Location = new System.Drawing.Point(143, 120);
            this.SpecificTextBox2.Name = "SpecificTextBox2";
            this.SpecificTextBox2.Size = new System.Drawing.Size(100, 20);
            this.SpecificTextBox2.TabIndex = 11;
            // 
            // FinishAddButton
            // 
            this.FinishAddButton.Location = new System.Drawing.Point(39, 226);
            this.FinishAddButton.Name = "FinishAddButton";
            this.FinishAddButton.Size = new System.Drawing.Size(204, 23);
            this.FinishAddButton.TabIndex = 12;
            this.FinishAddButton.Text = "Add Customer";
            this.FinishAddButton.UseVisualStyleBackColor = true;
            this.FinishAddButton.Click += new System.EventHandler(this.FinishAddButton_Click);
            // 
            // AddCustomerForm
            // 
            this.AcceptButton = this.FinishAddButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.FinishAddButton);
            this.Controls.Add(this.SpecificTextBox2);
            this.Controls.Add(this.SpecificTextBox1);
            this.Controls.Add(this.SpecificLabel2);
            this.Controls.Add(this.SpecificLabel1);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.AddCustPhone);
            this.Controls.Add(this.AddCustAddress);
            this.Controls.Add(this.AddCustName);
            this.Controls.Add(this.CustomerTypeLabel);
            this.Controls.Add(this.CustomerTypeDrop);
            this.Name = "AddCustomerForm";
            this.Text = "AddCustomerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CustomerTypeDrop;
        private System.Windows.Forms.Label CustomerTypeLabel;
        private System.Windows.Forms.Label AddCustName;
        private System.Windows.Forms.Label AddCustAddress;
        private System.Windows.Forms.Label AddCustPhone;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.Label SpecificLabel1;
        private System.Windows.Forms.Label SpecificLabel2;
        private System.Windows.Forms.TextBox SpecificTextBox1;
        private System.Windows.Forms.TextBox SpecificTextBox2;
        private System.Windows.Forms.Button FinishAddButton;
    }
}