﻿namespace WinFormsCustomersApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.customerListBox = new System.Windows.Forms.ListBox();
            this.SelectedName = new System.Windows.Forms.Label();
            this.SelectedAddress = new System.Windows.Forms.Label();
            this.SelectedPhone = new System.Windows.Forms.Label();
            this.SpecificField1 = new System.Windows.Forms.Label();
            this.SpecificField2 = new System.Windows.Forms.Label();
            this.CustomerAddButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customers:";
            // 
            // customerListBox
            // 
            this.customerListBox.FormattingEnabled = true;
            this.customerListBox.Location = new System.Drawing.Point(12, 36);
            this.customerListBox.Name = "customerListBox";
            this.customerListBox.Size = new System.Drawing.Size(167, 186);
            this.customerListBox.TabIndex = 1;
            this.customerListBox.SelectedIndexChanged += new System.EventHandler(this.customerListBox_SelectedIndexChanged);
            // 
            // SelectedName
            // 
            this.SelectedName.AutoSize = true;
            this.SelectedName.Location = new System.Drawing.Point(195, 36);
            this.SelectedName.Name = "SelectedName";
            this.SelectedName.Size = new System.Drawing.Size(38, 13);
            this.SelectedName.TabIndex = 2;
            this.SelectedName.Text = "Name:";
            // 
            // SelectedAddress
            // 
            this.SelectedAddress.AutoSize = true;
            this.SelectedAddress.Location = new System.Drawing.Point(195, 52);
            this.SelectedAddress.Name = "SelectedAddress";
            this.SelectedAddress.Size = new System.Drawing.Size(48, 13);
            this.SelectedAddress.TabIndex = 3;
            this.SelectedAddress.Text = "Address:";
            // 
            // SelectedPhone
            // 
            this.SelectedPhone.AutoSize = true;
            this.SelectedPhone.Location = new System.Drawing.Point(195, 69);
            this.SelectedPhone.Name = "SelectedPhone";
            this.SelectedPhone.Size = new System.Drawing.Size(41, 13);
            this.SelectedPhone.TabIndex = 4;
            this.SelectedPhone.Text = "Phone:";
            // 
            // SpecificField1
            // 
            this.SpecificField1.AutoSize = true;
            this.SpecificField1.Location = new System.Drawing.Point(195, 84);
            this.SpecificField1.Name = "SpecificField1";
            this.SpecificField1.Size = new System.Drawing.Size(0, 13);
            this.SpecificField1.TabIndex = 5;
            // 
            // SpecificField2
            // 
            this.SpecificField2.AutoSize = true;
            this.SpecificField2.Location = new System.Drawing.Point(194, 99);
            this.SpecificField2.Name = "SpecificField2";
            this.SpecificField2.Size = new System.Drawing.Size(0, 13);
            this.SpecificField2.TabIndex = 6;
            // 
            // CustomerAddButton
            // 
            this.CustomerAddButton.Location = new System.Drawing.Point(12, 226);
            this.CustomerAddButton.Name = "CustomerAddButton";
            this.CustomerAddButton.Size = new System.Drawing.Size(167, 23);
            this.CustomerAddButton.TabIndex = 7;
            this.CustomerAddButton.Text = "Add Customer";
            this.CustomerAddButton.UseVisualStyleBackColor = true;
            this.CustomerAddButton.Click += new System.EventHandler(this.CustomerAddButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 261);
            this.Controls.Add(this.CustomerAddButton);
            this.Controls.Add(this.SpecificField2);
            this.Controls.Add(this.SpecificField1);
            this.Controls.Add(this.SelectedPhone);
            this.Controls.Add(this.SelectedAddress);
            this.Controls.Add(this.SelectedName);
            this.Controls.Add(this.customerListBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox customerListBox;
        private System.Windows.Forms.Label SelectedName;
        private System.Windows.Forms.Label SelectedAddress;
        private System.Windows.Forms.Label SelectedPhone;
        private System.Windows.Forms.Label SpecificField1;
        private System.Windows.Forms.Label SpecificField2;
        private System.Windows.Forms.Button CustomerAddButton;

    }
}

