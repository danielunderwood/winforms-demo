﻿namespace WinFormsCustomersApp
{

    public class Customer
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public Customer() { }

        public Customer(string name, string address, string phone)
        {
            Name = name;
            Address = address;
            Phone = phone;
        }

        public override string ToString()
        {
            return Name + " " + Address + " " + Phone;
        }

        public class Company : Customer
        {
            public string Contact { get; set; }
            public int Discount { get; set; }

            public Company(string name, string address, string phone, string contact, int discount)
            {
                Name = name;
                Address = address;
                Phone = phone;
                Contact = contact;
                Discount = discount;
            }

            public override string ToString()
            {
                return Contact + " " + Discount.ToString();
            }
        }

        public class Individual : Customer
        {
            public string LicenseNumber { get; set; }

            public Individual(string name, string address, string phone, string licenseNumber)
            {
                Name = name;
                Address = address;
                Phone = phone;
                LicenseNumber = licenseNumber;
            }

            public override string ToString()
            {
                return LicenseNumber + " " + Name + " " + Address + " " + Phone;
            }
        }
    }
    
}
