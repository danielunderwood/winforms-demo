﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace WinFormsCustomersApp
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

            Text = "Sample Customer Application";

            SelectedName.Text = SelectedAddress.Text = SelectedPhone.Text = "";

            customerListBox.DataSource = Program.customerList;
            customerListBox.DisplayMember = "Name";

        }

        private void customerListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedName.Text = "Name: " + ((Customer)customerListBox.SelectedItem).Name;
            SelectedAddress.Text = "Name: " + ((Customer)customerListBox.SelectedItem).Address;
            SelectedPhone.Text = "Phone: " + ((Customer)customerListBox.SelectedItem).Phone;

            if (customerListBox.SelectedItem is Customer)
            {
                SpecificField1.Text = string.Empty;
                SpecificField2.Text = string.Empty;
            }

            var company = customerListBox.SelectedItem as Customer.Company;
            if (company != null)
            {
                SpecificField1.Text = "Contact: " + company.Contact;
                SpecificField2.Text = "Discount: " + company.Discount;
            }

            var selectedItem = customerListBox.SelectedItem as Customer.Individual;
            if (selectedItem != null)
            {
                SpecificField1.Text = "License Number: " +
                                      selectedItem.LicenseNumber;
                SpecificField2.Text = string.Empty;
            }
        }

        private void CustomerAddButton_Click(object sender, EventArgs e)
        {
            var addCustomerForm = new AddCustomerForm();
            addCustomerForm.Show();
        }
    }
}
