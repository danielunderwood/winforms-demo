﻿using System;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace WinFormsCustomersApp
{
    public partial class AddCustomerForm : Form
    {
        public AddCustomerForm()
        {
            InitializeComponent();

            Text = "Add Customer";

            CustomerTypeDrop.SelectedIndex = 0;
        }


        private void CustomerTypeDrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Customer")
            {
                SpecificLabel1.Visible = SpecificLabel2.Visible
                    = SpecificTextBox1.Visible = SpecificTextBox2.Visible = false;
            }

            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Company")
            {
                SpecificLabel1.Text = "Contact:";
                SpecificLabel2.Text = "Discount:";
                SpecificLabel1.Visible = SpecificLabel2.Visible
                    = SpecificTextBox1.Visible = SpecificTextBox2.Visible = true;
            }

            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Individual")
            {
                SpecificLabel2.Visible = SpecificTextBox2.Visible = false;
                SpecificLabel1.Text = "License Number:";
                SpecificLabel1.Visible = SpecificTextBox1.Visible = true;
            }

        }

        private void FinishAddButton_Click(object sender, EventArgs e)
        {
            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Customer")
            {
                Program.customerList.Add(new Customer
                    (NameTextBox.Text, AddressTextBox.Text, PhoneTextBox.Text));
                Close();
                
            }

            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Individual")
            {
                Program.customerList.Add(new Customer.Individual
                    (NameTextBox.Text, AddressTextBox.Text, PhoneTextBox.Text, SpecificTextBox1.Text));
                Close();
            }

            if (CustomerTypeDrop != null && (string)CustomerTypeDrop.SelectedItem == "Company")
            {

                try
                {
                    Program.customerList.Add(new Customer.Company
                                                 (NameTextBox.Text, AddressTextBox.Text, PhoneTextBox.Text,
                                                  SpecificTextBox1.Text, Convert.ToInt32(SpecificTextBox2.Text)));
                    Close();
                }
                catch (FormatException)
                {
                    MessageBox.Show("You may only use an integer value for the discount!");
                }
            }
        }
    }
}
